'''
The script records data coming form Joulescope, stores it into temp file.
After the capture finishes, file is procesed and statistics are generated.
The statistics are saved into .csv file.


example of json config file:
{
    "name":"EEP1 on solar cell",
    "root_file":"C:/Users/edin.omerovic/Desktop/Measurements/ATP_tests/jls_profiler1",
    "stats_filename":"stats.csv",
    "device_parameters":{
       "io_voltage":"3.3V",
       "buffer_duration":2,
       "current_lsb":"gpi0",
       "voltage_lsb":"gpi1",
       "source":"on",
       "i_range":"auto",
       "v_range":"",
       "turn_of_after_capture":false
    }
 }

by E.O.
'''

import argparse
import csv
import json
import time
from pathlib import Path
from loguru import logger
import datetime
import sys
import os

from joulescope import scan_require_one
from joulescope.data_recorder import DataReader

sys.path.insert(1, '../../../../ATP - Automated test platform/develop/automated-test-platform')
import modules.power.src.trigger_functions as trigger_fn
from modules.power.src.capture import Capture


_quit = False

def _current_time_str():
    t = datetime.datetime.utcnow()
    return t.isoformat()

def get_parser():
    p = argparse.ArgumentParser(
        description='Start recording sesion based on .json config')
    p.add_argument('root_path',
                   help='Location of .json config file')
    return p

class Listener:
    def __init__(self, device, config, capture):
        #Number of finished captures
        self._count = 0
        
        self._triggered = False
        self._bad_capture = False
        
        base_file_name = Path(config['root_file'])
        self._base_filename = os.path.join(base_file_name,  '')

        if "number_of_recordings" in config: self._total_recordings = config['number_of_recordings']
        else: self._total_recordings = None

        if 'initial_folder_name' in config: self._folder_name = config['initial_folder_name']
        else: self._folder_name = "" #Starting folder name

        self._sample_id_last = None
        self._time_start = None  # [sample_id, timestr]
        self._time_end = None

        self.stats_filename = config['stats_filename']

        self._current_capture = capture

        self._files_list = []

    def _construct_record_filename(self):
        return f"temp_{self._count}.jls"

    def start(self, start_id):
        #Listener detected interrupt needed for the start of current capture 
        logger.success(f"Started capture {self._count}")
        self._time_start = [start_id, _current_time_str()]
        
        self.filename = os.path.join(self._base_filename, str(self._folder_name))
        if not os.path.exists(self.filename): os.makedirs(self.filename)

        self.filename = os.path.join(self.filename, self._construct_record_filename())
        self._current_capture.start(start_id, self.filename)
        self._triggered = True

    def stop(self, end_id=None, forced = None):
        if not self._triggered: return
        #Listener detected interrupt needed for the start of current capture        
        self._time_end = [end_id, _current_time_str()]
        self._current_capture.stop(end_id)

        logger.success(f"Ended capture {self._count}")

        
        #self._current_capture = self._capture_list[self._count % len(self._capture_list)] #cyclic
        self._triggered = False
        self._files_list.append(self.filename)
        #----Teoretski ovdje bi trebao raditi procesiranje prethodnog capturea
        reader = DataReader()
        try:
            reader.open(self.filename)
        except:
            logger.error(f"Could not open file on location: {self.filename}")
            reader.close()

        beginning, end = reader.sample_id_range
        stats = reader.statistics_get(beginning, end)         
        reader.close()
        #Write to csv: count, delta t, energy, average current
        timestamp_str = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        with open(os.path.join(self._base_filename , self.stats_filename), 'a+', newline="") as f:
            w = csv.writer(f)
            w.writerow([timestamp_str, 
                        self._count,
                        stats['time']['delta']['value'],
                        stats['signals']['power']['∫']['value'],
                        stats['signals']['current']['µ']['value']])

        #Delete the temp file
        if len(self._files_list) >= 4:
            os.remove(self._files_list[0])
            del self._files_list[0]

        self._count += 1

    def close(self):
        if not self._triggered: return
        self._current_capture.stop()
        self._triggered = False
        os.remove(self.filename)
        print(self.filename)
        for files in self._files_list: os.remove(files)

  
    def __call__(self, stream_buffer):
        if(self._total_recordings is not None and
           self._count >= self._total_recordings):
            return True #Stop the recording process

        start_id, end_id = stream_buffer.sample_id_range

        if self._sample_id_last is not None and start_id < self._sample_id_last:
            start_id = self._sample_id_last
        if start_id >= end_id:
            return False  # nothing to process

        while start_id < end_id:
            self._start_fn = getattr(trigger_fn, self._current_capture._start_trigger)
            self._end_fn = getattr(trigger_fn, self._current_capture._end_trigger)

            if not self._triggered:
                trigger_id = self._start_fn(stream_buffer, self, start_id, end_id)                
                if trigger_id is None:
                    start_id = end_id #Nothing detected
                else:
                    logger.info(f"Start trigger detected. Type: {self._current_capture._start_trigger}, trigger_id: {trigger_id}")
                    self.start(trigger_id)
                    start_id = trigger_id + 1
            else:
                trigger_id = self._end_fn(stream_buffer, self, self._sample_id_last, end_id)

                if trigger_id is None: #Didn't find end trigger
                    response = self._current_capture._add(stream_buffer, self._sample_id_last, end_id)
                    start_id = end_id

                else: #Found end trigger
                    if self._sample_id_last + 2 < trigger_id: #Add the remaining samples from the buffer
                        response = self._current_capture._add(stream_buffer, self._sample_id_last, trigger_id - 1)
                    else: 
                        self._sample_id_last = trigger_id + 1
                        continue #If the capture is 1 sample long, ignore it

                    if not response:
                        logger.error("Force stopped the capture: missing samples")
                        self.stop(start_id, forced= True)
                    else:
                        logger.info(f"Stop trigger detected. Type: {self._current_capture._end_trigger}, trigger_id: {trigger_id}")
                        self.stop(trigger_id)
                        start_id = trigger_id + 1

            self._sample_id_last = start_id
        
        self._sample_id_last = end_id
        return False
  

if __name__ == "__main__":
    logger.success("Opened jls_profiler.py script")
    args = get_parser().parse_args()

    logger.info(f"Attempting to open config file of path: {args.root_path}")
    if os.path.exists(args.root_path):
        f = open(args.root_path)
        config = json.load(f)
        logger.success("Opened .json config file")
    else:
        logger.error(f"Could not find .json config file on specified location")
        raise RuntimeError("Json config file does not exist")

    device = scan_require_one(config='ignore')

    with device:
        logger.info("Configuring the Device")
        for parameter, value in config['device_parameters'].items():
            if parameter == "turn_of_after_capture":
                power_off = value
                continue
            if value:  device.parameter_set(parameter, value)

        logger.info("Configuring the Captures")
        capture_config = {
                        "capture_type":"lowres",
                        "capture_name":"C1",
                        "start_trigger":"_start_falling",
                        "end_trigger":"_end_rising",
                        "gpio_channel":"in1",
                        "downsampling":10
        }
        cap = Capture(device, capture_config)

        logger.info("Configuring the Listener")
        listen = Listener(device = device,
                          config = config,
                          capture=cap)

        logger.info("Starting the recording session")
        device.start()
        while not _quit:
            try:
                if listen(device.stream_buffer):
                    break
            except KeyboardInterrupt:
                logger.info("Capture ended by CTRL + C")
                _quit = True
                cap.close()
                listen.close()
                device.stop()
        
        logger.info("Stopping the recording session")
        if power_off:
            device.parameter_set('i_range', 'off')
            logger.info("Turning off the DUT")
