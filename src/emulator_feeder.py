'''
This script takes .csv timeseries file with illumination data 
and feeds the data to the pyboard controller of solar emulator

example for running the script:
python emulator_feeder.py config.json

example of config.json:

{
    "filepath":"C:/Users/edin.omerovic/Desktop/Internship project/!enocean_projects/power_management_estimation/measurements/IlluminationReport_2022-2-2(17-34).csv",
    "coef" : 1,
    "time_scale": 1000,
    "port":"COM6",
    "baud_rate":115200
}


by E.O.
'''
import time, threading
import argparse
import serial
import json
import os

import pandas as pd
from loguru import logger

def get_parser():
    p = argparse.ArgumentParser(
        description='Start recording sesion based on .json config')
    p.add_argument('root_path',
                   help='Location of .json config file')
    return p


class Feeder:
    def __init__(self, config):
        self.filepath = config['filepath']
        self.time_scale = config['time_scale']
        self.next_time = time.time()
        self.serial = serial.Serial(config['port'], config['baud_rate'])
        self.coef = config['coef']
        self.counter = 0
    
    def _send_data(self, data):
        data = data/self.coef
        logger.info(data)
        command = "SE_set_brightness " + str(data) + " $"
        self.serial.write(bytes(command, 'unicode_escape'))


    def __call__(self):
        df = pd.read_csv(self.filepath)
        
        if self.counter >= len(df) - 1:
            logger.info(".csv list feeded")
            os._exit(1)        
        
        value = df.iloc[self.counter, 1]
        self._send_data(value)
        
        wait_time = df.iloc[self.counter + 1, 0] - df.iloc[self.counter, 0]
        if wait_time < 0: RuntimeError(".csv time negative")
        wait_time = wait_time/self.time_scale
        self.next_time += wait_time 

        threading.Timer(self.next_time - time.time(), self.__call__).start()
        self.counter += 1



if __name__ == "__main__":
    logger.success("Opened emulator_feeder.py script")
    args = get_parser().parse_args()

    logger.info(f"Attempting to open config file of path: {args.root_path}")
    if os.path.exists(args.root_path):
        f = open(args.root_path)
        config = json.load(f)
        logger.success("Opened .json config file")
    else:
        logger.error(f"Could not find .json config file on specified location")
        raise RuntimeError("Json config file does not exist")

    feed = Feeder(config)

    feed()
    while 1:
        try: time.sleep(0.1)
        except KeyboardInterrupt:
            logger.info("Capture ended by CTRL + C")
            os._exit(1)