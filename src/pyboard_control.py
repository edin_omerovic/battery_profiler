import pyb, micropython
import random
micropython.alloc_emergency_exception_buf(100)

global random8
global random_range

#Go through all pins and turn them off or on acordingly
#numer=8 disables the mux
def write_adress(number):
    if number == 8: en_pin.low()
    else: en_pin.high() 
    for i, pin in enumerate(adress_pins):
        if number % 2 == 1: pin.high()
        else: pin.low()
        number = int(number / 2)


def switch_callback():
    global load_enable_flag, undervoltage_treshold
    load_enable_flag = not(load_enable_flag)
    undervoltage_treshold = 0

def convert(temp):
    return (temp/4095)*3.3

def generateRandomPulse(random8, random_range):
    write_adress(random8)
    pyb.delay(random_range)

def take_adc():
    global undervoltage_treshold
    sig_pin.high()
    battery_voltage = convert(v_bat.read())
    if battery_voltage <= UNDERVOLTAGE_TRESHOLD: undervoltage_treshold += 1
    print(battery_voltage)
    pyb.delay(SIGNAL_PULSE_DURATION)
    sig_pin.low()
    #Generiši puls

def mode2():
    counter = 0
    first_time = True
    while 1:
        if not load_enable_flag or undervoltage_treshold >= UNTERVOLTAGE_COUNT:
            rec_pin.low()
            sig_pin.high()
            write_adress(8)
            if not first_time and undervoltage_treshold == 0: take_adc()
            first_time = True
            continue
        
        if first_time:
            take_adc()
            first_time = False

        rec_pin.high()
        sig_pin.low()
        random8 = random.randint(0,7) #Random channel
        min, max = intervals[random8]
        random_range = random.randint(min,max) #Random time
        
        generateRandomPulse(random8, random_range)
        
        #Dont count pauses in the load counting
        if random8 != 0: counter += 1
        
        if counter >= RECORD_TRESHOLD:
            write_adress(8) #Disable load
            pyb.delay(VOLTAGE_SETTLE_DOWN)
            take_adc()
            counter = 0
        
        if undervoltage_treshold >= UNTERVOLTAGE_COUNT: print("stop")
        



if __name__ == "__main__":
    #Every how many times afte generating random pulses you take measurements of Vbat
    RECORD_TRESHOLD = 30
    VOLTAGE_SETTLE_DOWN = 10000
    SIGNAL_PULSE_DURATION = 1000
    UNDERVOLTAGE_TRESHOLD = 2.1
    UNTERVOLTAGE_COUNT = 4
    
    load_enable_flag = False
    undervoltage_treshold = 0

    #Intervals in multiply by 0.1ms
    intervals = {
                0 : [500, 3000], #not connected
                1 : [500, 1000], #5.1k
                2 : [500, 1000], #3.3k
                3 : [100, 500], #2k
                4 : [100, 500], #1k
                5 : [10, 50], #Telegrams low : 620 ohm
                6 : [10, 20], #Telegrams high: 330 ohm
                7 : [1, 5], #PEAKS: 100 ohm
                }

    rec_pin = pyb.Pin('X1', pyb.Pin.OUT_PP)
    adress_pins = [pyb.Pin('X2', pyb.Pin.OUT_PP), #A0
                pyb.Pin('X3', pyb.Pin.OUT_PP), #A1
                pyb.Pin('X4', pyb.Pin.OUT_PP)] #A2

    en_pin = pyb.Pin('X5', pyb.Pin.OUT_PP)
    sig_pin = pyb.Pin('X6', pyb.Pin.OUT_PP)
    v_bat = pyb.ADC(pyb.Pin('X22'))
    
    sw = pyb.Switch()
    sw.callback(switch_callback)

    mode2()
