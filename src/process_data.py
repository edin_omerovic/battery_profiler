#Importujes data koji je specificiran u konfiguracijskom fajlu
#Importovanje .csv i .jls fajla
#Iz .jls fajla izvučes samo data koji ima in0 high
#Izvuci informacija o potrošenoj energiji

#CSV snimak posmatraš cijeli
#U momentu kada se digne in0 posmatraj jls

#Prvi dio

'''
Povezivanje data korištenjem timestampeova
'''

import json
import csv
import sys
import os
import argparse
from numpy import average, diff
import pandas as pd
from dateutil.parser import parse
sys.path.insert(1, '../../../../ATP - Automated test platform/develop/automated-test-platform')
#import modules.power.src.processing as processing
import matplotlib.pyplot as plt

from loguru import logger

def get_parser():
    p = argparse.ArgumentParser(
        description='Start recording sesion based on .json config')
    p.add_argument('root_path',
                   help='Location of .json config file')
    return p

def get_stats(file_path, trigger_channel):
    reader = DataReader()
    try:
        reader.open(file_path)
    except:
        logger.error(f"Could not open file on location: {file_path}")
        reader.close()

    trigger_channel = processing.FIELD_MAP.get(trigger_channel, trigger_channel) #Convert in0 to current_lsb and in1 to voltage_lsb
    s_min, s_max = reader.sample_id_range
    samples = reader.samples_get(s_min, s_max, 'samples')
    in0 = samples['signals'][trigger_channel]['value']
    edges = processing.detect_edges(in0) #List of indexes (PROBLEMATICNO)
    new_edges = []
    for i in range(len(edges)-1):
        if edges[i+1] - edges[i] > 4:
            new_edges.append(edges[i])
    
    new_edges.pop(0)
    energies, times, currents = [], [], []
    for i in range(len(new_edges)-1):
        if i % 2 == 1:
            stat_data = reader.statistics_get(new_edges[i], new_edges[i + 1])
            #print(stat_data['signals']['power']['∫']['value'])
            times.append(stat_data['time']['delta']['value'])
            energies.append(stat_data['signals']['power']['∫']['value'])
            currents.append(stat_data['signals']['current']['µ']['value'])
    reader.close()
    return energies, times, currents

def mode1():
    if not os.path.isfile("stat_output.csv"):
        energies, times, currents = get_stats(jsl_file, "in1")
        f = open("stat_output.csv", 'w', newline="", encoding="utf-8")
        w = csv.writer(f)
        for i in range(len(energies)):
            w.writerow([energies[i], times[i], currents[i]])


    stas = pd.read_csv("stat_output.csv", header=None)
    df = pd.read_csv(output_file, header=None)

    x_energies = [0]

    for i in range(len(stas[0])):
        x_energies.append(x_energies[-1] - stas[0][i])
    
    plt.plot(df[1][1:], x_energies[1:])
    plt.xlabel("Battery volrage [V]")
    plt.ylabel("Energy extracted from battery [J]")
    plt.show()


def mode2():
    jls_file = "C:/Users/edin.omerovic/Desktop/experimetn 17.02.2022/edited_data.csv" 
    pyb_file = "C:/Users/edin.omerovic/Desktop/experimetn 17.02.2022/output.csv"

    stats = pd.read_csv(jls_file, header=None)
    voltages = pd.read_csv(pyb_file, header=None)
    
    print(stats)
    print(voltages)

    energies = stats[2] 
    vbat = voltages[1] 

    y_energies = [0]
    for i in range(len(energies)):
        y_energies.append(y_energies[-1] - energies[i])

    plt.plot(vbat[:-2],y_energies)
    plt.show()


def pair_values(pyb_list, jls_list):
    if len(pyb_list) == len(jls_list): return
    epsilon_us = 100000
    offset = 0
    return_list = []
    for i,x in enumerate(pyb_list):
        jls_time = parse(jls_list[i + offset])
        pyb_time = parse(x)
        if (jls_time - pyb_time).microseconds > epsilon_us:
            while True:
                next_time = parse(jls_list[i + offset])
                if pyb_time < next_time: break
                offset += 1
        return_list.append((i,i + offset))
    return return_list


def _merge_values(list):
    return [list[0].iloc[-1], list[1].iloc[-1], sum(list[2]), sum(list[3]), average(list[4])]

def merge(jls, pairs):
    new_list = []
    for i, pair in enumerate(pairs[:-1]):
        current_pair = pair
        next_pair = pairs[i + 1]
        x1, y1 = current_pair
        x2, y2 =  next_pair
        if y2 - y1 > 1:
            merged = _merge_values(jls[y1:y2])
            #print(current_pair, next_pair)
            #print(merged)
            new_list.append(merged)
            continue
        #Append to the new array
        new_list.append(jls.iloc[y1].tolist())
        #print(jls.iloc[y1].tolist())
    #print(jls.iloc[y2].tolist())
    new_list.append(jls.iloc[y2].tolist())
    return pd.DataFrame(new_list)

def run():
    logger.success("Opened jls_profiler.py script")
    args = get_parser().parse_args()

    logger.info(f"Attempting to open config file of path: {args.root_path}")
    if os.path.exists(args.root_path):
        f = open(args.root_path)
        config = json.load(f)
        logger.success("Opened .json config file")
    else:
        logger.error(f"Could not find .json config file on specified location")
        raise RuntimeError("Json config file does not exist")

    jls_file = pd.read_csv(config['jls_file'], header=None)
    pyb_file = pd.read_csv(config['pyb_file'], header=None)

    #Uporedi fajlove po broju uzoraka
    #Za groudn truth koristi pyboard_file
    jsl_times = jls_file[0]
    pyb_times = pyb_file[0]
    #Remove the first element before pairing
    pyb_times_s = list(pyb_times[1:])
    #Find pairs based on pyboard data
    pairs = pair_values(pyb_times_s, jsl_times)
    #Create new jsl data based on data matched with pyboard 
    new_jls_file = merge(jls_file, pairs)
    
    voltages = pyb_file[1]
    energy = new_jls_file[3]

    x_energies = [0]
    for x in energy:
        x_energies.append(x_energies[-1] - x)

    plt.plot(voltages, x_energies)
    plt.xlabel("Battery volrage [V]")
    plt.ylabel("Energy extracted from battery [J]")
    plt.show()



if __name__ == "__main__":
    run()
