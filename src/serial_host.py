'''
Script for reading the data gathered form PyBoard

example for running:
python serial_host.py serial_config.json

example json file:
{
    "port":"COM6",
    "baud_rate":115200,
    "exit_key" : "f3",
    "record_key" : "f4",
    "output_filepath" : "../data/output2.csv"
}

by E.O.
'''
import argparse
import keyboard
import datetime
import serial
import time
import json
import csv
import os

from loguru import logger

save_to_file = False
run = True

def get_parser():
    p = argparse.ArgumentParser(
        description='Start recording sesion based on .json config')
    p.add_argument('root_path',
                   help='Location of .json config file')
    return p

def record_toggle(key):
    global save_to_file
    logger.info("You pressed record")
    save_to_file = not save_to_file
    if save_to_file: logger.success("Starting recording")
    else: logger.success("Stoping recording")

def exit_program(key):
    global run
    logger.info("Exiting the program")
    run = False
    quit()
            
if __name__ == "__main__":
    logger.success("Opened jls_profiler.py script")
    args = get_parser().parse_args()

    logger.info(f"Attempting to open config file of path: {args.root_path}")
    if os.path.exists(args.root_path):
        f = open(args.root_path)
        config = json.load(f)
        logger.success("Opened .json config file")
    else:
        logger.error(f"Could not find .json config file on specified location")
        raise RuntimeError("Json config file does not exist")

    if os.path.exists(config['port']) == True:
        ser = serial.Serial(config['port'], config['baud_rate'])
        time.sleep(1)
    else:
        logger.error(f"Could not find .json config file on specified location")
        exit()
    
    keyboard.on_press_key(config['record_key'], record_toggle)
    keyboard.on_press_key(config['exit_key'], exit_program)
    output_filepath = config['output_filepath'] 

    while run:
        if ser.inWaiting() > 0:
            data = ser.readline()
            data = data.decode("utf-8","ignore")
            if data[:-2] == "stop": exit_program('')            
            st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

            if save_to_file:
                f = open(output_filepath, 'a+', newline="", encoding='utf-8')
                writer = csv.writer(f)
                writer.writerow([st,data[:-2]])
                f.close()
                print("*", end=" ")
            print(f"{st}, data: {data[:-2]}")
